package com.log320.sudoku;

import java.util.Date;

/**
 * Created by MarcOlivier on 2014-09-29.
 */
public class SudokuOutputFormat {

    private final String TOP_AND_BOTTOM_SUDOKU_LINE = "+-------+-------+-------+";

    public SudokuOutputFormat(){}

    public void printExecutionTime(Date t1, Date t2){
        System.out.println("Temps d'exécution : " + (t2.getTime() - t1.getTime()) + " millisecondes");
    }

    public void printNoSolutionFound(){
        System.out.println("No solution found.");
    }

    public void printSolution(int[][] solution){

        for (int i = 0; i < Sudoku.NUMBER_OF_ROW; i++) {
            if(i % Sudoku.SUB_GRID_SIZE ==0){
                System.out.println(TOP_AND_BOTTOM_SUDOKU_LINE);
            }

            StringBuffer line = new StringBuffer("|");
            for (int j = 0; j < Sudoku.NUMBER_OF_COL; j++) {
                if( j == 3 || j == 6){
                    line.append(" |");
                }

                line.append(" ");

                if(solution[i][j] == 0){
                    line.append(" ");
                }
                else {
                    line.append(Integer.toString(solution[i][j]));
                }
            }
            line.append(" |");

            System.out.println(line.toString());
        }
        System.out.println(TOP_AND_BOTTOM_SUDOKU_LINE);
    }
}

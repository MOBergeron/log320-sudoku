package com.log320.sudoku;

import java.io.*;

/**
 * Created by MarcOlivier on 2014-09-29.
 */
public class FileManager {

    public FileManager(){

    }

    public int[][] readFile(String path) throws IOException, Exception{
        BufferedReader bufferedReader = null;
        int[][] content = new int[Sudoku.NUMBER_OF_ROW][Sudoku.NUMBER_OF_COL];
        String eachLine = "";

        try{
            bufferedReader = new BufferedReader(new FileReader(path));
            eachLine = bufferedReader.readLine();

            for(int i = 0; i < Sudoku.NUMBER_OF_ROW; i++){
                if(eachLine == null){
                    throw new Exception("Le fichier ne contient pas assez de ligne. Il doit en contenir " + Sudoku.NUMBER_OF_ROW);
                }

                if(eachLine.length() < Sudoku.NUMBER_OF_COL){
                    throw new Exception("Une ligne du fichier contient moins de "+ Sudoku.NUMBER_OF_COL +" caractères");
                }

                if(eachLine.length() > Sudoku.NUMBER_OF_COL){
                    content[i] = convertStringToInt(eachLine.substring(0,Sudoku.NUMBER_OF_COL));
                }else{
                    content[i] = convertStringToInt(eachLine);
                }

                eachLine = bufferedReader.readLine();
            }
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        finally{
            if(bufferedReader != null){
                bufferedReader.close();
            }
        }
        return content;
    }

    private int[] convertStringToInt(String stringToConvert) throws Exception{
        int[] converted = new int[Sudoku.NUMBER_OF_COL];

        int i = 0;
        for(Character c : stringToConvert.toCharArray()){
            if(!Character.isDigit(c)){
                throw new Exception("A character in the file is not a digit.");
            }
            converted[i] = Character.getNumericValue(c);
            i++;
        }

        return converted;
    }
}

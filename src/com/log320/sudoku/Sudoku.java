package com.log320.sudoku;

import java.util.Arrays;

/**
 * Created by Marc on 2014-09-29.
 */
public class Sudoku {

    public static final int NUMBER_OF_ROW = 9;
    public static final int NUMBER_OF_COL = 9;
    public static final int SUB_GRID_SIZE = 3;

    private int[][] grid = new int[NUMBER_OF_ROW][NUMBER_OF_COL];

    public Sudoku(int grid[][]) {
        this.grid = grid.clone();
    }


    /**
     * Returns true of the given value is valid at a specific position in the grid
     *
     * @param row   Row of the grid
     * @param col   Column of the grid
     * @param value Value to validate
     * @return true if value is defined at x,y position
     */
    public boolean isValid(int row, int col, int value, int[][] tmpGrid) {
        boolean isValid = true;

        for (int i = 0; i < NUMBER_OF_COL; i++) {
            if (tmpGrid[row][i] == value) {
                isValid = false;
                break;
            } else if (tmpGrid[i][col] == value) {
                isValid = false;
                break;
            }
        }

        if (isValid) {
            int rowStart = row - row%3;
            int colStart = col - col%3;

            for (int i = 0; i < SUB_GRID_SIZE; i++) {
                for (int j = 0; j < SUB_GRID_SIZE; j++) {
                    if (tmpGrid[rowStart + i][colStart + j] == value) {
                        isValid = false;
                        break;
                    }
                }
            }
        }


        return isValid;
    }

    public void setValue(int row,int col, int value){
        grid[row][col] = value;
    }

    public int getValue(int row,int col){
        return grid[row][col];
    }

    public boolean solve(int row, int col, int[][] tmpGrid){
        if(getValue(row, col) != 0){
            return solveNextCase(row, col, tmpGrid);
        }else {
            for (int i = 1; i <= 9; i++) {
                if (isValid(row, col, i, tmpGrid)) {
                    tmpGrid[row][col] = i;
                    if(solveNextCase(row, col, tmpGrid)){
                        return true;
                    }
                }
            }
        }

        tmpGrid[row][col] = 0;
        return false;
    }

    private boolean solveNextCase(int row, int col, int[][] tmpGrid){
        int tmpCol;
        int tmpRow;

        if (col == NUMBER_OF_COL - 1) {
            tmpCol = 0;
            tmpRow = row + 1;
        } else {
            tmpCol = col + 1;
            tmpRow = row;
        }

        if (tmpRow == NUMBER_OF_ROW) {
            grid = tmpGrid.clone();
            return true;
        }

        if (solve(tmpRow, tmpCol, tmpGrid)) {
            return true;
        }

        return false;
    }

    public int [][] getSudokuGrid(){
        return grid;
    }
}
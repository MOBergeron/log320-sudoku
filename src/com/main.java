package com;

import com.log320.sudoku.FileManager;
import com.log320.sudoku.SudokuOutputFormat;
import com.log320.sudoku.Sudoku;

import java.io.IOException;
import java.util.Date;

public class main {
    public static void main(String[] args) {
        SudokuOutputFormat of = new SudokuOutputFormat();
        FileManager fm = new FileManager();
        Sudoku sudoku;

        try {
            int[][] content = fm.readFile("sudoku.txt");
            sudoku = new Sudoku(content);

            Date temps = new Date();
            if(sudoku.solve(0,0, content.clone())){
                Date t2 = new Date();
                of.printSolution(sudoku.getSudokuGrid());
                of.printExecutionTime(temps, t2);
            }
            else{
                of.printNoSolutionFound();
            }

        }catch(IOException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }


    }
}
